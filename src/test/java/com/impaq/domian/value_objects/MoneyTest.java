package com.impaq.domian.value_objects;

import static org.junit.Assert.*;

import org.junit.Test;

import com.impaq.domian.value_objects.Money;

public class MoneyTest {

	@Test
	public void shouldEqualsReturnsTrueWhenValuesAreTheSame() {
		// given
		Money money1 = Money.of("10.20", "EUR");
		Money money2 = Money.of("10.20", "EUR");

		// then
		assertTrue(money1.equals(money2));
		assertTrue(money2.equals(money1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenTryToCompareValuesWithDifferentCurrencies() {
		// given
		Money money1 = Money.of("10.50", "EUR");
		Money money2 = Money.of("10.50", "PLN");

		// when
		money1.compareTo(money2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenTryToAddValuesWithDifferentCurrencies() {
		// given
		Money money1 = Money.of("10.00", "EUR");
		Money money2 = Money.of("10.00", "PLN");

		// when
		money1.add(money2);
	}

	@Test
	public void shouldCorrectlyAddTwoValuesWithIndenticalCurrency() {
		// given
		Money twoAndHalfPln = Money.of("2.5", "PLN");
		Money sevenAndSixtyThreePln = Money.of("7.63", "PLN");

		// when
		Money sum = twoAndHalfPln.add(sevenAndSixtyThreePln);

		// then
		Money correctSum = Money.of("10.13", "PLN");
		int correctCompareToResult = 0;

		assertSame(correctSum.compareTo(sum), correctCompareToResult);
	}

	@Test
	public void shouldHashCodeReturnTrueWhenValuesAreTheSame() {
		// given
		Money money1 = Money.of("10.20", "PLN");
		Money money2 = Money.of("10.20", "PLN");

		// then
		assertEquals(money1.hashCode(), money2.hashCode());
	}

	@Test
	public void shouldHashCodeReturnFalseWhenMoneyAreDiffrentValues() {
		// given
		Money money1 = Money.of("10.20", "PLN");
		Money money2 = Money.of("15.20", "PLN");

		// then
		assertNotEquals(money1.hashCode(), money2.hashCode());
	}

	@Test
	public void shouldHashCodeReturnFalseWhenMoneyAreDiffrentCurrency() {
		// given
		Money money1 = Money.of("10.20", "EUR");
		Money money2 = Money.of("10.20", "PLN");

		// then
		assertNotEquals(money1.hashCode(), money2.hashCode());
	}
}