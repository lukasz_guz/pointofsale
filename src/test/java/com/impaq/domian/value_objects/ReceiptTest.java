package com.impaq.domian.value_objects;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.impaq.domian.entities.Product;

public class ReceiptTest {

	private Receipt receipt;

	@Before
	public void setUp() {
		receipt = new Receipt();

		Product p = mock(Product.class);
		when(p.getSummation()).thenReturn("Rolki 20 z�");
		when(p.getPrice()).thenReturn(Money.of("20", "PLN"));
		receipt.addProduct(Optional.of(p));

		p = mock(Product.class);
		when(p.getSummation()).thenReturn("�y�wy 15 z�");
		when(p.getPrice()).thenReturn(Money.of("15", "PLN"));
		receipt.addProduct(Optional.of(p));
	}

	@Test
	public void shouldReturnExpectedSummation() {
		// given
		String expectedSummation = "--- Receipt ---\n" + "Rolki 20 z�\n" + "�y�wy 15 z�\n" + "Total price: 35,00 z�";

		// when
		String summation = receipt.getSummation();

		// then
		assertEquals(expectedSummation, summation);
	}

	@Test
	public void shouldAddProduct() {
		// given
		Product p = mock(Product.class);
		when(p.getPrice()).thenReturn(Money.of("10", "PLN"));
		int startSize = receipt.getProducts().size();

		// when
		receipt.addProduct(Optional.of(p));
		int actualSize = receipt.getProducts().size();

		// then
		assertEquals(startSize + 1, actualSize);
	}

	@Test
	public void shouldReturnTotalPrice() {
		// then
		assertEquals("Total price: 35,00 z�", receipt.getTotalPriceSummation());
	}
}