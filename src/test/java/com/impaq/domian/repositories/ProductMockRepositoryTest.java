package com.impaq.domian.repositories;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.impaq.domian.entities.Product;

public class ProductMockRepositoryTest {

	private ProductRepository repository;

	@Before
	public void setUp() {
		repository = new ProductMockRepository();
	}

	@Test
	public void shouldFindProductById() {
		// given
		Long productId = 1L;

		// when
		Optional<Product> productFromRepository = repository.findById(productId);
		Product product = productFromRepository.get();

		// then
		assertEquals(productId, product.getId());
	}

	@Test
	public void shouldFindProductByBarCode() {
		// given
		String barCode = "12AV";

		// when
		Optional<Product> productFromRepository = repository.findByBarCode(barCode);
		Product product = productFromRepository.get();

		// then
		assertEquals(barCode, product.getBarCode());
	}

	@Test
	public void shouldReturnEmptyOptionalWhenProductIsNotFound() {
		// given
		String barCode = "0000";

		// when
		Optional<Product> productFromRepository = repository.findByBarCode(barCode);

		// then
		assertFalse(productFromRepository.isPresent());
	}
}
