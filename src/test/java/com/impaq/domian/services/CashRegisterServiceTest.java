package com.impaq.domian.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.impaq.devices.input.BarCodeReader;
import com.impaq.devices.output.OutputDevice;
import com.impaq.domian.entities.Product;
import com.impaq.domian.repositories.ProductRepository;
import com.impaq.domian.value_objects.Money;
import com.impaq.domian.value_objects.Receipt;
import com.impaq.domian.value_objects.SaleContext;

@RunWith(MockitoJUnitRunner.class)
public class CashRegisterServiceTest {

	@Mock
	private ProductRepository productRepository;

	@InjectMocks
	private CashRegisterService service;

	@Before
	public void setUp() throws Exception {
		service = CashRegisterService.getInstance();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldDisplayProductWhenScannerFindBarCode() throws IOException {
		// given
		String barCode = "A123";
		BarCodeReader barCodeReader = mock(BarCodeReader.class);
		when(barCodeReader.read()).thenReturn(barCode);

		OutputDevice lcd = mock(OutputDevice.class);
		OutputDevice printer = mock(OutputDevice.class);

		Product product = mock(Product.class);
		when(product.getSummation()).thenReturn("�y�wy 10,00 PLN");
		when(product.getPrice()).thenReturn(Money.of("10", "PLN"));

		when(productRepository.findByBarCode(barCode)).thenReturn(Optional.of(product));

		SaleContext context = new SaleContext();
		context.barCode = barCode;
		context.lcds.add(lcd);
		context.printers.add(printer);

		// when
		service.scanProductToReceipt(barCodeReader, context);

		// then
		verify(productRepository).findByBarCode(barCode);
		verify(lcd).print(product.getSummation());
		verifyZeroInteractions(printer);
	}

	@Test
	public void shouldDisplayReceipt() throws IOException {
		// given
		String barCode = "A123";
		BarCodeReader barCodeReader = mock(BarCodeReader.class);
		when(barCodeReader.read()).thenReturn(barCode);

		OutputDevice lcd = mock(OutputDevice.class);
		OutputDevice printer = mock(OutputDevice.class);

		Product product = mock(Product.class);
		when(product.getSummation()).thenReturn("�y�wy 10,00 PLN");
		when(product.getPrice()).thenReturn(Money.of("10", "PLN"));

		when(productRepository.findByBarCode(barCode)).thenReturn(Optional.of(product));

		Receipt receipt = new Receipt();

		SaleContext context = new SaleContext();
		context.barCode = barCode;
		context.receipt = receipt;
		context.printRecipt = true;
		context.lcds.add(lcd);
		context.printers.add(printer);

		// when
		service.scanProductToReceipt(barCodeReader, context);

		// then
		verify(productRepository).findByBarCode(barCode);
		verify(lcd).print("Total price: 10,00 z�");
		verify(printer).print("--- Receipt ---\n�y�wy 10,00 PLN\nTotal price: 10,00 z�");
		verifyZeroInteractions(printer);

		assertFalse(context.printRecipt);
		assertNotSame(receipt, context.receipt);
	}

	@Test
	public void shouldDisplayErrorWhenScannerFindEmptyBarCode() throws IOException {
		// given
		String barCode = null;
		BarCodeReader barCodeReader = mock(BarCodeReader.class);
		when(barCodeReader.read()).thenReturn(barCode);

		OutputDevice lcd = mock(OutputDevice.class);
		OutputDevice printer = mock(OutputDevice.class);

		when(productRepository.findByBarCode(barCode)).thenReturn(Optional.empty());

		SaleContext context = new SaleContext();
		context.barCode = barCode;
		context.lcds.add(lcd);
		context.printers.add(printer);

		// when
		service.scanProductToReceipt(barCodeReader, context);

		// then
		verify(productRepository).findByBarCode(barCode);
		verify(lcd).error("Invalid bar-code");
		verifyZeroInteractions(printer);
	}

	@Test
	public void shouldDisplayErrorWhenScannerNotFoundProduct() throws IOException {
		// given
		String barCode = "AAA";
		BarCodeReader barCodeReader = mock(BarCodeReader.class);
		when(barCodeReader.read()).thenReturn(barCode);

		OutputDevice lcd = mock(OutputDevice.class);
		OutputDevice printer = mock(OutputDevice.class);

		when(productRepository.findByBarCode(barCode)).thenReturn(Optional.empty());

		SaleContext context = new SaleContext();
		context.barCode = barCode;
		context.lcds.add(lcd);
		context.printers.add(printer);

		// when
		service.scanProductToReceipt(barCodeReader, context);

		// then
		verify(productRepository).findByBarCode(barCode);
		verify(lcd).error("Product not found");
		verifyZeroInteractions(printer);
	}
}