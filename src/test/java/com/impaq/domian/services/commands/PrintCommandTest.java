package com.impaq.domian.services.commands;

import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Spy;

import com.impaq.devices.output.OutputDevice;
import com.impaq.domian.entities.Product;
import com.impaq.domian.value_objects.Money;
import com.impaq.domian.value_objects.Receipt;
import com.impaq.domian.value_objects.SaleContext;

public class PrintCommandTest {

	private PrintCommand command;

	@Spy
	private Receipt receipt;

	@Before
	public void setUp() {
		command = new PrintCommand();
		receipt = new Receipt();

		Product p = mock(Product.class);
		when(p.getSummation()).thenReturn("Rolki 20 z�");
		when(p.getPrice()).thenReturn(Money.of("20", "PLN"));
		receipt.addProduct(Optional.of(p));

		p = mock(Product.class);
		when(p.getSummation()).thenReturn("�y�wy 15 z�");
		when(p.getPrice()).thenReturn(Money.of("15", "PLN"));
		receipt.addProduct(Optional.of(p));
	}

	@Test
	public void shouldPrintReceiptSummation() {
		// given
		OutputDevice lcd = mock(OutputDevice.class);
		OutputDevice printer = mock(OutputDevice.class);

		SaleContext context = new SaleContext();
		context.receipt = receipt;
		context.printRecipt = true;
		context.lcds.add(lcd);
		context.printers.add(printer);

		// when
		command.process(context);

		// then
		verify(lcd).print(receipt.getTotalPriceSummation());
		verify(printer).print("--- Receipt ---\nRolki 20 z�\n�y�wy 15 z�\nTotal price: 35,00 z�");
	}

	@Test
	public void shouldPrintProduct() {
		// given
		OutputDevice lcd = mock(OutputDevice.class);
		OutputDevice printer = mock(OutputDevice.class);

		Product p = mock(Product.class);
		Optional<Product> pOptional = Optional.of(p);

		SaleContext context = new SaleContext();
		context.product = pOptional;
		context.receipt = receipt;
		context.lcds.add(lcd);
		context.printers.add(printer);

		// when
		command.process(context);

		// then
		verify(lcd).print(p.getSummation());
		verifyZeroInteractions(printer);
	}

	@Test
	public void shouldNothningToDoWhenProductIsEmpty() {
		// given
		OutputDevice lcd = mock(OutputDevice.class);
		OutputDevice printer = mock(OutputDevice.class);

		SaleContext context = new SaleContext();
		context.product = Optional.empty();
		context.receipt = receipt;
		context.lcds.add(lcd);
		context.printers.add(printer);

		// when
		command.process(context);

		// then
		verifyZeroInteractions(lcd);
		verifyZeroInteractions(printer);
	}
}