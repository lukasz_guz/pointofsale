package com.impaq.domian.services.commands;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.impaq.devices.output.OutputDevice;
import com.impaq.domian.entities.Product;
import com.impaq.domian.value_objects.SaleContext;

public class ProductNotFoundCommandTest {

	private ProductNotFoundCommand command;

	@Before
	public void setUp() throws Exception {
		command = new ProductNotFoundCommand();
	}

	@Test
	public void shouldInvokeErrorOnOutputDeviceWhenProductIsNotFound() {
		// given
		OutputDevice outputDevice = mock(OutputDevice.class);

		SaleContext context = new SaleContext();
		context.product = Optional.empty();
		context.lcds.add(outputDevice);

		// when
		command.process(context);

		// then
		verify(outputDevice).error("Product not found");
	}

	@Test
	public void shouldNothningToDoWhenProductIsFound() {
		// given
		OutputDevice outputDevice = mock(OutputDevice.class);

		Product product = mock(Product.class);

		SaleContext context = new SaleContext();
		context.product = Optional.of(product);
		context.lcds.add(outputDevice);

		// when
		command.process(context);

		// then
		verifyZeroInteractions(outputDevice);
	}
}