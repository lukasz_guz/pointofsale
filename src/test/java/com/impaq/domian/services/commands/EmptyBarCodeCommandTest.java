package com.impaq.domian.services.commands;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import com.impaq.devices.output.OutputDevice;
import com.impaq.domian.value_objects.SaleContext;

public class EmptyBarCodeCommandTest {

	private EmptyBarCodeCommand command;

	@Before
	public void setUp() throws Exception {
		command = new EmptyBarCodeCommand();
	}

	@Test
	public void shouldInvokeErrorOnOutputDeviceWhenBarCodeIsNull() {
		// given
		OutputDevice outputDevice = mock(OutputDevice.class);

		SaleContext context = new SaleContext();
		context.barCode = null;
		context.lcds.add(outputDevice);

		// when
		command.process(context);

		// then
		verify(outputDevice).error("Invalid bar-code");
	}

	@Test
	public void shouldNothningToDoWhenBarCodeIsNotNull() {
		// given
		OutputDevice outputDevice = mock(OutputDevice.class);

		SaleContext context = new SaleContext();
		context.barCode = "AVCD";
		context.lcds.add(outputDevice);

		// when
		command.process(context);

		// then
		verifyZeroInteractions(outputDevice);
	}
}