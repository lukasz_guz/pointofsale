package com.impaq.domian.services.commands;

import static org.junit.Assert.*;

import org.junit.Test;

import com.impaq.domian.value_objects.Receipt;
import com.impaq.domian.value_objects.SaleContext;

public class CleanContextCommandTest {

	@Test
	public void shouldCleanSaleContexWhenPrintReciptIsTrue() {
		// given
		CleanContextCommand cleanContext = new CleanContextCommand();
		SaleContext context = new SaleContext();
		context.printRecipt = true;

		Receipt receipt = new Receipt();
		context.receipt = receipt;

		// when
		cleanContext.process(context);

		// then
		assertFalse(context.printRecipt);
		assertNotSame(receipt, context.receipt);
	}

	@Test
	public void shouldNothingToDoWhenPrintReciptIsFalse() {
		// given
		CleanContextCommand cleanContext = new CleanContextCommand();
		SaleContext context = new SaleContext();
		context.printRecipt = false;

		Receipt receipt = new Receipt();
		context.receipt = receipt;

		// when
		cleanContext.process(context);

		// then
		assertFalse(context.printRecipt);
		assertSame(receipt, context.receipt);
	}
}