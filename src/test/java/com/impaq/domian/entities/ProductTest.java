package com.impaq.domian.entities;

import static org.junit.Assert.*;

import org.junit.Test;

import com.impaq.domian.value_objects.Money;

public class ProductTest {

	@Test
	public void shouldHashCodeReturnTrueWhenValuesAreTheSame() {
		// given
		Product product1 = new Product(1L, "name", "barCode", Money.of("10", "PLN"));
		Product product2 = new Product(1L, "name", "barCode", Money.of("10", "PLN"));

		// then
		assertEquals(product1.hashCode(), product2.hashCode());
	}

	@Test
	public void shouldHashCodeReturnFalseWhenProductsAreDiffrentValues() {
		// given
		Product product1 = new Product(1L, "name1", "barCode", Money.of("10", "PLN"));
		Product product2 = new Product(1L, "name2", "barCode", Money.of("10", "PLN"));

		// then
		assertNotEquals(product1.hashCode(), product2.hashCode());
	}

	@Test
	public void shouldEqualsReturnsTrueWhenValuesAreTheSame() {
		// given
		Product product1 = new Product(1L, "name", "barCode", Money.of("10", "PLN"));
		Product product2 = new Product(1L, "name", "barCode", Money.of("10", "PLN"));

		// then
		assertTrue(product1.equals(product2));
		assertTrue(product2.equals(product1));
	}

	@Test
	public void shouldReturnExpectedSummation() {
		// given
		Product product = new Product(1L, "name", "barCode", Money.of("10", "PLN"));
		String expectedSummation = "name\tbarCode\t10,00 z�";

		// when
		String summation = product.getSummation();

		// then
		assertEquals(expectedSummation, summation);
	}
}
