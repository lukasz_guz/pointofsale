package com.impaq;

import java.util.Observable;
import java.util.Scanner;

public class ExitListener extends Observable implements Runnable {

	@Override
	public void run() {
		try (Scanner scan = new Scanner(System.in);) {
			while (true) {
				String line = scan.nextLine();
				if ("exit".equals(line)) {
					setChanged();
					notifyObservers();
				}
			}
		}
	}
}