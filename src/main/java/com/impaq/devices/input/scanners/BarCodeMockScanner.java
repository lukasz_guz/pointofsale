package com.impaq.devices.input.scanners;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.impaq.devices.input.BarCodeReader;

public class BarCodeMockScanner implements BarCodeReader {

	private List<String> fakeBarCodes = new ArrayList<String>(Arrays.asList("A298", "12F3", "777B", null, "12AV", "15CD"));

	private Random randomGenerator = new Random();

	@Override
	public String read() {
		sleep3seconds();
		int bound = fakeBarCodes.size() - 1;
		return fakeBarCodes.get(randomGenerator.nextInt(bound));
	}

	private void sleep3seconds() {
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
}