package com.impaq.devices.input;

public interface BarCodeReader {

	public String read();
}