package com.impaq.devices.output;


public interface OutputDevice {

	public void print(String data);

	public void error(String error);
}