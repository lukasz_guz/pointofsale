package com.impaq.devices.output.displays;

import com.impaq.devices.output.OutputDevice;

public class Lcd implements OutputDevice {

	@Override
	public void print(String data) {
		System.out.println("LCD: " + data);
	}

	@Override
	public void error(String error) {
		System.err.println(error);
	}
}