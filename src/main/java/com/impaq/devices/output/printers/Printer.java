package com.impaq.devices.output.printers;

import com.impaq.devices.output.OutputDevice;

public class Printer implements OutputDevice {

	@Override
	public void print(String data) {
		System.out.println("Printer :" + data);
	}

	@Override
	public void error(String error) {
		System.err.println(error);
	}
}