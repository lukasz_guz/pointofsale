package com.impaq.domian.entities;

import java.util.Objects;

import com.impaq.domian.value_objects.Money;
import com.impaq.domian.value_objects.Summation;

public class Product implements Summation {

	private Long id;

	private String name;

	private String barCode;

	private Money price;

	public Product() {
		super();
	}

	public Product(Long id, String name, String barCode, Money price) {
		super();
		this.id = id;
		this.name = name;
		this.barCode = barCode;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public Money getPrice() {
		return price;
	}

	public void setPrice(Money price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, barCode, price);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (barCode == null) {
			if (other.barCode != null)
				return false;
		} else if (!barCode.equals(other.barCode))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}

	@Override
	public String getSummation() {
		return name + "\t" + barCode + "\t" + price;
	}
}