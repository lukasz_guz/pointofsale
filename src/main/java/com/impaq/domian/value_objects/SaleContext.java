package com.impaq.domian.value_objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.impaq.devices.output.OutputDevice;
import com.impaq.domian.entities.Product;

public class SaleContext {

	public String barCode;
	public Optional<Product> product;
	public Receipt receipt = new Receipt();
	public boolean printRecipt = false;
	public List<OutputDevice> lcds = new ArrayList<OutputDevice>();
	public List<OutputDevice> printers = new ArrayList<OutputDevice>();
}