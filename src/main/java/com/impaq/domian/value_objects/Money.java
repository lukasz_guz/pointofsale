package com.impaq.domian.value_objects;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Objects;

public class Money implements Comparable<Money> {

	private BigDecimal amount;

	private Currency currency;

	private Money(final BigDecimal amount, final Currency currency) {
		this.amount = amount;
		this.currency = currency;
	}

	public Money(final String amount, final String currencyCode) {
		this.amount = new BigDecimal(amount);
		this.currency = Currency.getInstance(currencyCode);
	}

	public static Money of(final String amount, final String currencyCode) {
		return new Money(amount, currencyCode);
	}

	public Money add(final Money augend) {
		checkCurrency(augend, "Cannot add money with different currencies!");

		final BigDecimal totalAmount = amount.add(augend.amount);
		return new Money(totalAmount, currency);
	}

	public BigDecimal getAmout() {
		return amount;
	}

	public String getCurrencyCode() {
		return currency.getCurrencyCode();
	}

	private String getCurrencyFormat() {
		final NumberFormat numberFormat = DecimalFormat.getCurrencyInstance();
		final int fractionDigits = currency.getDefaultFractionDigits();
		numberFormat.setCurrency(currency);
		numberFormat.setMinimumFractionDigits(fractionDigits);
		numberFormat.setMinimumFractionDigits(fractionDigits);
		return numberFormat.format(amount);
	}

	@Override
	public String toString() {
		return getCurrencyFormat();
	}

	@Override
	public int compareTo(final Money other) {
		checkCurrency(other, "Cannot compare money with different currencies!");

		return amount.compareTo(other.amount);
	}

	@Override
	public int hashCode() {
		return Objects.hash(amount, currency);
	}

	@Override
	public boolean equals(final Object other) {
		if (other == null) {
			return false;
		}
		if (other.getClass() != getClass()) {
			return false;
		}
		final Money money = (Money) other;
		if (!this.amount.equals(money.amount)) {
			return false;
		}
		if (!this.currency.equals(money.currency)) {
			return false;
		}
		return true;
	}

	private void checkCurrency(final Money other, final String message) {
		if (!currency.equals(other.currency)) {
			throw new IllegalArgumentException(message);
		}
	}
}