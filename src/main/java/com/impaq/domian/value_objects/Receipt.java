package com.impaq.domian.value_objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.impaq.domian.entities.Product;

public class Receipt implements Summation {

	private List<Product> products = new ArrayList<>();
	private Money totalPrice = Money.of("0", "PLN");

	public void addProduct(Optional<Product> product) {
		product.ifPresent((p) -> {
			products.add(p);
			totalPrice = totalPrice.add(p.getPrice());
		});
	}

	@Override
	public String getSummation() {
		String summation = "--- Receipt ---\n";
		for (Product product : products) {
			summation += product.getSummation() + "\n";
		}
		summation += "Total price: " + totalPrice.toString();
		return summation;
	}

	public List<Product> getProducts() {
		return products;
	}

	public String getTotalPriceSummation() {
		return "Total price: " + totalPrice;
	}
}