package com.impaq.domian.services.commands;

import com.impaq.domian.value_objects.Receipt;
import com.impaq.domian.value_objects.SaleContext;

public class CleanContextCommand implements ProcessSaleCommand {

	@Override
	public void process(SaleContext context) {
		if (context.printRecipt) {
			context.receipt = new Receipt();
			context.printRecipt = false;
		}
	}
}