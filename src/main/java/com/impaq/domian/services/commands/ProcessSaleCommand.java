package com.impaq.domian.services.commands;

import com.impaq.domian.value_objects.SaleContext;

public interface ProcessSaleCommand {

	void process(SaleContext context);
}