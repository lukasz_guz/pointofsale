package com.impaq.domian.services.commands;

import com.impaq.domian.value_objects.SaleContext;

public class PrintCommand implements ProcessSaleCommand {

	@Override
	public void process(SaleContext context) {
		if (context.printRecipt) {
			printRecipt(context);
		} else if (context.product.isPresent()) {
			printProduct(context);
		}
	}

	private void printRecipt(SaleContext context) {
		context.lcds.stream().forEach((device) -> device.print(context.receipt.getTotalPriceSummation()));
		context.printers.forEach((device) -> device.print(context.receipt.getSummation()));
	}

	private void printProduct(SaleContext context) {
		context.lcds.stream().forEach((device) -> device.print(context.product.get().getSummation()));
	}
}