package com.impaq.domian.services.commands;

import java.util.Objects;

import com.impaq.domian.value_objects.SaleContext;

public class EmptyBarCodeCommand implements ProcessSaleCommand {

	private static final String NULL_BARCODE_ERROR_MESSAGE = "Invalid bar-code";

	@Override
	public void process(SaleContext context) {
		if (Objects.isNull(context.barCode)) {
			context.lcds.stream().forEach((device) -> device.error(NULL_BARCODE_ERROR_MESSAGE));
		}
	}
}