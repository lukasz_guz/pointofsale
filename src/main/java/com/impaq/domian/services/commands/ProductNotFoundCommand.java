package com.impaq.domian.services.commands;

import com.impaq.domian.value_objects.SaleContext;

public class ProductNotFoundCommand implements ProcessSaleCommand {

	private static final String PRODUCT_NOT_FOUND_ERROR_MESSAGE = "Product not found";

	@Override
	public void process(SaleContext context) {
		if (!context.product.isPresent()) {
			context.lcds.stream().forEach((device) -> device.error(PRODUCT_NOT_FOUND_ERROR_MESSAGE));
		}
	}
}