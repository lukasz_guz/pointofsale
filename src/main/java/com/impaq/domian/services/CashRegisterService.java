package com.impaq.domian.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.impaq.devices.input.BarCodeReader;
import com.impaq.domian.entities.Product;
import com.impaq.domian.repositories.ProductMockRepository;
import com.impaq.domian.repositories.ProductRepository;
import com.impaq.domian.services.commands.CleanContextCommand;
import com.impaq.domian.services.commands.EmptyBarCodeCommand;
import com.impaq.domian.services.commands.PrintCommand;
import com.impaq.domian.services.commands.ProcessSaleCommand;
import com.impaq.domian.services.commands.ProductNotFoundCommand;
import com.impaq.domian.value_objects.SaleContext;

public class CashRegisterService implements PointOfSaleService {

	private static final CashRegisterService INSTANCE = getInstance();

	private ProductRepository productRepository;
	private List<ProcessSaleCommand> commands;

	private CashRegisterService() {
		productRepository = new ProductMockRepository();
		commands = new ArrayList<>(Arrays.asList(new EmptyBarCodeCommand(), new ProductNotFoundCommand(), new PrintCommand(), new CleanContextCommand()));
	}

	public static CashRegisterService getInstance() {
		if (INSTANCE == null) {
			return new CashRegisterService();
		}
		return INSTANCE;
	}

	@Override
	public void scanProductToReceipt(BarCodeReader barCodeReader, SaleContext context) {
		String barCode = barCodeReader.read();
		Optional<Product> productOptional = productRepository.findByBarCode(barCode);
		context.product = productOptional;
		context.receipt.addProduct(productOptional);
		commands.forEach((command) -> command.process(context));
	}
}