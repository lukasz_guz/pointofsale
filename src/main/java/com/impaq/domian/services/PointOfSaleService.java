package com.impaq.domian.services;

import com.impaq.devices.input.BarCodeReader;
import com.impaq.domian.value_objects.SaleContext;

public interface PointOfSaleService {
	void scanProductToReceipt(BarCodeReader barCodeReader, SaleContext context);
}