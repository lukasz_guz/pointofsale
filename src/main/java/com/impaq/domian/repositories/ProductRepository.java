package com.impaq.domian.repositories;

import java.util.Optional;

import com.impaq.domian.entities.Product;

public interface ProductRepository {

	Optional<Product> findById(Long id);

	Optional<Product> findByBarCode(String barCode);
}