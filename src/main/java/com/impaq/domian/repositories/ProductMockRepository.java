package com.impaq.domian.repositories;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import com.impaq.domian.entities.Product;
import com.impaq.domian.value_objects.Money;

public class ProductMockRepository implements ProductRepository {

	private Set<Product> fakeProducts = new HashSet<Product>(Arrays.asList(new Product(1L, "�y�ka", "A298", Money.of("1.99", "PLN")), new Product(2L, "Opona",
			"12F3", Money.of("200.00", "PLN")), new Product(3L, "Kosz na �mieci", "777B", Money.of("15.99", "PLN")),
			new Product(4L, "Zapa�ki", "12AV", Money.of("0.10", "PLN"))));

	@Override
	public Optional<Product> findById(Long id) {
		return fakeProducts.stream().filter((product) -> product.getId().equals(id)).findFirst();
	}

	@Override
	public Optional<Product> findByBarCode(String barCode) {
		return fakeProducts.stream().filter((product) -> product.getBarCode().equals(barCode)).findFirst();
	}
}