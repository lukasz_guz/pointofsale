package com.impaq;

import java.util.Observable;
import java.util.Observer;

import com.impaq.devices.input.BarCodeReader;
import com.impaq.devices.input.scanners.BarCodeMockScanner;
import com.impaq.devices.output.displays.Lcd;
import com.impaq.devices.output.printers.Printer;
import com.impaq.domian.services.CashRegisterService;
import com.impaq.domian.services.PointOfSaleService;
import com.impaq.domian.value_objects.SaleContext;

/**
 * 
 * @author Lukasz Guz
 * 
 *         Symulacja po wpisaniu s�owa exit w konsoli drukuje paragon. 
 *
 */
public class PointOfSaleMain implements Observer {

	private boolean printRecipt = false;
	private SaleContext context;
	private BarCodeReader barCodeReader;
	private PointOfSaleService service;
	
	public PointOfSaleMain() {
		context = initContext(new SaleContext());
		barCodeReader = new BarCodeMockScanner();
		service = CashRegisterService.getInstance();
	}
	
	public void run() {
		service.scanProductToReceipt(barCodeReader, context);
		printRecipt();
	}
	

	public static void main(String[] args) {
		PointOfSaleMain main = new PointOfSaleMain();

		ExitListener exitLister = new ExitListener();
		exitLister.addObserver(main);

		Thread listenerThread = new Thread(exitLister);
		listenerThread.start();

		while (true) {
			main.run();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		this.printRecipt = true;
	}

	private SaleContext initContext(SaleContext context) {
		context.lcds.add(new Lcd());
		context.printers.add(new Printer());
		return context;
	}

	private void printRecipt() {
		if (this.printRecipt) {
			this.context.printRecipt = true;
		}
	}
}